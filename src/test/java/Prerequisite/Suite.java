package Prerequisite;
import utilities.config;

import org.testng.ITestContext;
import org.testng.annotations.BeforeSuite;

public class Suite 
{
	@BeforeSuite
	public void Initilization(ITestContext ctx)
	{
		
		config.init();
		config.log.debug(new String(new char[100]).replace("\0", "-"));
		config.log.debug("Suite name: "+ctx.getCurrentXmlTest().getSuite().getName());
		
	}
	}
