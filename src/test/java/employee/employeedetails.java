package employee;
import static io.restassured.RestAssured.given;
import java.util.Hashtable;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Prerequisite.Suite;
import ReusableCode.auth;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;
import utilities.DataHandler;
import utilities.config;


public class employeedetails extends Suite{ 
	public static ResponseSpecification responseSpec;
	@BeforeTest //Logging the test case details before starting
	public void PreTestProcess() 
	{
		config.log.debug(new String(new char[100]).replace("\0", "-"));
		config.log.debug(this.getClass().getName()+ " Entered");
	}
	
@Test(dataProviderClass = DataHandler.class,dataProvider="dataProvider") // Validate Status code is 200
	public void Assert200(Hashtable<String,String> dataTable) {
	RestAssured.baseURI = config.property.getProperty("LocalServer"); // Getting the server path from the properties file
	responseSpec = auth.reuseAssert200();	
	given().when().get(dataTable.get("EndPoint")).then().spec(responseSpec);//Validating the status code through ResponseSpecBuilder
}

@Test(dataProviderClass = DataHandler.class,dataProvider="dataProvider") // Validate header Content Type
public void jsonheader(Hashtable<String,String> dataTable) {
RestAssured.baseURI = config.property.getProperty("LocalServer"); 
Response response=given().when().get(dataTable.get("EndPoint"));
String contenttype = response.header("Content-Type");
Assert.assertEquals(contenttype , dataTable.get("contenttype"));
}	
@Test(dataProviderClass = DataHandler.class,dataProvider="dataProvider") // Validate Successfull Assertions
public void successfullAssertion(Hashtable<String,String> dataTable) {
RestAssured.baseURI = config.property.getProperty("LocalServer"); 
Response response=given().when().get(dataTable.get("EndPoint"));
JsonPath jsonPathEvaluator = response.jsonPath();
String ages = jsonPathEvaluator.getString("employeeData.age[0]");
Assert.assertEquals(ages, dataTable.get("Age")); 
String status = jsonPathEvaluator.getString("status");
Assert.assertEquals(status, dataTable.get("Status"));
String role = jsonPathEvaluator.getString("employeeData.role[0]");
Assert.assertEquals(role, dataTable.get("Role"));
String dob = jsonPathEvaluator.getString("employeeData.dob[0]");
Assert.assertEquals(dob, dataTable.get("Dob"));
String message = jsonPathEvaluator.getString("message");
Assert.assertEquals(message, dataTable.get("Message"));
}
@Test(dataProviderClass = DataHandler.class,dataProvider="dataProvider") // Failed Assetion
public void failedAssertion(Hashtable<String,String> dataTable) {
RestAssured.baseURI = config.property.getProperty("LocalServer"); 
Response response=given().when().get(dataTable.get("EndPoint"));
JsonPath jsonPathEvaluator = response.jsonPath();
String company = jsonPathEvaluator.getString("employeeData.company[0]");
Assert.assertEquals(company, dataTable.get("Company")); 
}	

	@AfterTest//Logging the test case details after test case is executed
	public void PostTestProcess() 
	{
		config.log.debug(this.getClass().getName()+ "  Exited");
		config.log.debug(new String(new char[100]).replace("\0", "-"));
	}
	}

  		


