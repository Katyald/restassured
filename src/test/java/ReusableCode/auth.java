package ReusableCode;
import static org.hamcrest.Matchers.lessThan;
import java.util.concurrent.TimeUnit;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.ResponseSpecification;
import utilities.config;

public class auth { 
	public static ResponseSpecification responseSpec;
	
	
	

public static  ResponseSpecification reuseAssert200(){
	
	ResponseSpecBuilder builder = new ResponseSpecBuilder();	
	builder.expectStatusCode(200);
	responseSpec = builder.build();
	return responseSpec;
}
	
public static ResponseSpecification reuseAssert401(){
	
	ResponseSpecBuilder builder = new ResponseSpecBuilder();	
	builder.expectStatusCode(401);
	builder.expectResponseTime(lessThan(8000L), TimeUnit.MILLISECONDS);
	responseSpec = builder.build();
	return responseSpec;
}


public static ResponseSpecification reuseAssert400(){
	
	ResponseSpecBuilder builder = new ResponseSpecBuilder();		
	builder.expectStatusCode(400);
	builder.expectResponseTime(lessThan(8000L), TimeUnit.MILLISECONDS);
	responseSpec = builder.build();
	return responseSpec;
	
}

public static ResponseSpecification reuseAssert404(){
	
	ResponseSpecBuilder builder = new ResponseSpecBuilder();		
	builder.expectStatusCode(404);
	builder.expectResponseTime(lessThan(8000L), TimeUnit.MILLISECONDS);
	responseSpec = builder.build();
	return responseSpec;
	
}

public static ResponseSpecification reuseAssert409(){
	
	ResponseSpecBuilder builder = new ResponseSpecBuilder();		
	builder.expectStatusCode(409);
	builder.expectResponseTime(lessThan(8000L), TimeUnit.MILLISECONDS);
	responseSpec = builder.build();
	return responseSpec;
	
}

public static ResponseSpecification reuseAssert500(){
	
	ResponseSpecBuilder builder = new ResponseSpecBuilder();		
	builder.expectStatusCode(500);
	builder.expectResponseTime(lessThan(4000L), TimeUnit.MILLISECONDS);
	responseSpec = builder.build();
	return responseSpec;
	
}
}

